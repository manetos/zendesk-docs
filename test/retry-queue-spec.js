'use strict';

const assert = require('assert');

const sinon = require('sinon');

const RetryQueue = require('../lib/retry-queue');

describe('RetryQueue', function() {
    it('should execute tasks in order', function(done) {
        const que = new RetryQueue();
        const spy = sinon.spy();
        que.push(() => {
            spy('first');
            return Promise.resolve(RetryQueue.result());
        });
        que.push(() => {
            spy('second');
            return Promise.resolve(RetryQueue.result());
        });
        que.on('completed', () => {
            assert.equal(spy.getCall(0).args, 'first');
            assert.equal(spy.getCall(1).args, 'second');
            done();
        });
    });
    it('should retry task', function(done) {
        const que = new RetryQueue();
        const work = sinon.stub()
            .onCall(0).returns(Promise.resolve(RetryQueue.result('res', 10)))
            .onCall(1).returns(Promise.resolve(RetryQueue.result('res', false)));
        que.push(work);
        que.on('completed', () => {
            assert(work.calledTwice);
            done();
        });
    });
    it('should execute tasks in order even with a retry', function(done) {
        const que = new RetryQueue();
        const spy = sinon.spy();
        const retryWork = sinon.stub()
            .onCall(0).returns(Promise.resolve(RetryQueue.result('res', 10)))
            .onCall(1).returns(Promise.resolve(RetryQueue.result('res', false)));

        que.push(() => {
            spy('first');
            return Promise.resolve(RetryQueue.result());
        });
        que.push(() => {
            spy('retry');
            return retryWork();
        });
        que.push(() => {
            spy('last');
            return Promise.resolve(RetryQueue.result());
        });
        que.on('completed', () => {
            assert.equal(spy.getCall(0).args, 'first');
            assert.equal(spy.getCall(1).args, 'retry');
            assert.equal(spy.getCall(2).args, 'retry');
            assert.equal(spy.getCall(3).args, 'last');
            done();
        });
    });
    it('should sleep for defined retry time', function(done) {
        const que = new RetryQueue();

        let lastRun = Date.now();
        function getDuration() {
            const now = Date.now();
            const duration = now - lastRun;
            lastRun = now;
            return duration;
        }

        const callDurations = [0, 10, 20, 30, 40, 0];
        let retryCall = 1;

        que.push(() => {
            const duration = getDuration();
            const expectedDelay = callDurations[retryCall-1];
            assert(duration >= expectedDelay,
                `Found duration ${duration} instead of ${expectedDelay}`);
            return Promise.resolve(RetryQueue.result('res', callDurations[retryCall++]));
        });

        que.on('completed', () => {
            done();
        });
    });
    it('should return a promise with the result of the task', function() {
        const que = new RetryQueue();
        return que.push(() => Promise.resolve(RetryQueue.result('the result')))
            .then(res => assert.equal(res, 'the result'));
    });
    it('should return a promise with the result of the retried task', function() {
        const que = new RetryQueue();
        const retryWork = sinon.stub()
            .onCall(0).returns(Promise.resolve(RetryQueue.result('failed result', 10)))
            .onCall(1).returns(Promise.resolve(RetryQueue.result('success result', false)));
        return que.push(retryWork)
            .then(res => assert.equal(res, 'success result'));
    });
});

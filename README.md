This is a small utility to publish markdown documents to Zendesk Guide.

It tracks and uploads markdown documents to zendesk. It keeps track of the
article id in the header of the source language file.

The tool in its current form can upload articles and their translations. It
also creates categories and sections however the categories and sections are
not locale-aware, resulting in a bit of a mess where translations cannot easily
be found in the UI.

# Current state

We have abandoned the use of this tool currently but welcome any contributions.

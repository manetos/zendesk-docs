'use strict';

const fs = require('fs');
const path = require('path');
const execFile = require('child_process').execFile;

const _ = require('lodash');
const matter = require('gray-matter');
const logger = require('winston');

class Markdown {
    constructor(file) {
        this.file = file;
    }
    convertToHtml(content) {
        return new Promise((resolve, reject) => {
            const child = execFile('pandoc', [], (error, stdout, stderr) => {
                if (stderr) {
                    logger.error('pandoc stderr: \n' + stderr);
                }
                if (error) {
                    return reject(error);
                }
                resolve(stdout);
            });

            child.stdin.end(content);
        });
    }
    readFile() {
        return new Promise((resolve, reject) => {
            fs.readFile(this.file, {
                encoding: 'utf8'
            }, (err, data) => {
                if (err) {
                    return reject(err);
                }
                resolve(data);
            });
        });
    }
    writeFile(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(this.file, data, {
                encoding: 'utf8'
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        });
    }
    getMarkdownContents() {
        return this.readFile().then(data => matter(data));
    }
    getContents() {
        return this.getMarkdownContents()
            .then(doc => this.convertToHtml(doc.content)
                .then(html => _.defaults({ html: html }, doc))
            );
    }
    updateFrontMatter(data) {
        return this.getMarkdownContents()
            .then(doc => matter.stringify(doc.content, _.defaults(data, doc.data)))
            .then(fileData => this.writeFile(fileData));
    }
    getFileName() {
        return path.basename(this.file);
    }
}

module.exports = Markdown;

'use strict';

const FAQ_CATEGORY_ID = 200453252;

class Sections {
    constructor(zendesk) {
        this.zendesk = zendesk;
        this.cachedSections = {};
    }
    getSection(locale) {
        if (locale in this.cachedSections) {
            return Promise.resolve(this.cachedSections[locale]);
        }
        return this.zendesk.getSectionsForLocale(locale)
            .then(reply => {
                this.cachedSections[locale] = reply;
                return reply;
            });
    }
    getSectionId(locale, sectionName) {
        return this.getSection(locale)
            .then(reply => {
                return reply.sections
                    .filter(section => section.name === sectionName)
                    .map(section => section.id)[0];
            });
    }
    ensureSectionsExist(locale, ...sectionNames) {
        return this.getSection(locale)
            .then(reply => {
                const sections = reply.sections;
                const existingSections = new Set(sections.map(section => section.name));
                const missingSections = difference(new Set(sectionNames), existingSections);
                return Promise.all(
                    Array.from(missingSections).map(sectionName =>
                        this.zendesk.createSection(locale, FAQ_CATEGORY_ID, sectionName))
                );
            });
    }
}

function difference(setA, setB) {
    var difference = new Set(setA);
    for (const elem of setB) {
        difference.delete(elem);
    }
    return difference;
}

module.exports = Sections;

'use strict';

class Document {
    constructor(markdownFile) {
        this.file = markdownFile;
    }
    isSourceDocument() {
        return this.file.getContents()
            .then(contents => contents.data.locale)
            .then(locale => locale === 'en-us');
    }
    getDocumentName() {
        return this.isSourceDocument()
            .then(isSourceDocument => {
                const filename = this.file.getFileName();
                if (isSourceDocument) {
                    return filename;
                }
                else {
                    return filename.replace(/-..\.md$/, '.md');
                }
            });
    }
}

module.exports = Document;

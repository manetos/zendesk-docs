'use strict';

const _ = require('lodash');

const Article = require('./article');
const Document = require('./document');
const FileFinder = require('./file-finder');
const Markdown = require('./markdown');

class DocumentFinder {
    constructor(path) {
        this.path = path;
    }
    findDocuments() {
        const fileFinder = new FileFinder(this.path);
        return fileFinder.getMarkdownFiles()
            .then(files => Promise.all(files.map(getDocument)))
            .then(files => files.filter(isValidFile))
            .then(docs => _(docs)
                .groupBy(doc => doc.name)
                .mapValues(docs => {
                    const sourceDoc = docs.find(doc => doc.isSourceDocument);
                    const translations = docs.filter(doc => !doc.isSourceDocument);
                    return new Article(sourceDoc, translations);
                })
                .values()
                .value()
            );
    }
}

function getDocument(file) {
    const markdownFile = new Markdown(file);
    const doc = new Document(markdownFile);
    return Promise.all([
        doc.isSourceDocument().then(isSourceDocument => ({ isSourceDocument })),
        doc.getDocumentName().then(name => ({ name })),
        markdownFile.getMarkdownContents()
    ]).then(results => _.defaults(...results, { file: markdownFile }));
}

function isValidFile(file) {
    return file.data.locale && file.data.title && file.data.section;
}

module.exports = DocumentFinder;


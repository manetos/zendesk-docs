'use strict';

class Article {
    constructor(sourceDoc, translationDocs) {
        this.source = sourceDoc;
        this.translations = translationDocs;
    }
}

class Publisher {
    constructor(article, zendesk, sections) {
        this.article = article;
        this.zendesk = zendesk;
        this.sections = sections;
    }
    isPublished() {
        const articleId = this.article.source.data.articleId;
        return Promise.resolve(typeof articleId === 'number');
    }
    updateOrPublish() {
        return this.isPublished().then(isPublished => {
            if (isPublished) {
                return this.updateArticle();
            }
            return this.publishArticle();
        });
    }
    updateArticle() {
        return Promise.all([
            this.article.source.file.getContents()
                .then(contents => this.updateTranslation(contents)),
            this.updateOrPublishTranslations()
        ]);
    }
    publishArticle() {
        return this.article.source.file.getContents()
            .then(doc => this.createArticle(doc.data.locale, doc.data.section, {
                title: doc.data.title,
                locale: doc.data.locale,
                body: doc.html,
            })).then(article => {
                const articleId = article.article.id;
                this.article.source.file.updateFrontMatter({ articleId });
                // FIXME this is not very pretty, we should preferably not modify articles
                this.article.source.data.articleId = articleId;
                return article;
            }).then(() => this.updateOrPublishTranslations());
    }
    createArticle(locale, sectionName, article) {
        return this.sections.getSectionId(locale, sectionName)
            .then(section => this.zendesk.createArticle(section, article));
    }
    updateOrPublishTranslations() {
        return Promise.all(this.article.translations.map(
            t => this.updateOrPublishTranslation(t.file)
        ));
    }
    updateOrPublishTranslation(translation) {
        translation.getContents()
            .then(contents => this.doesTranslationExist(contents.data.locale)
                .then(translationExists => {
                    if (translationExists) {
                        return this.updateTranslation(contents);
                    }
                    return this.createTranslation(contents);
                })
            );
    }
    doesTranslationExist(locale) {
        const articleId = this.article.source.data.articleId;
        return this.zendesk.getTranslation(articleId, locale)
            .then(() => true)
            .catch(res => {
                if (res.statusCode === 404) {
                    return false;
                }
                return Promise.reject(res);
            });
    }
    updateTranslation(doc) {
        const id = this.article.source.data.articleId;
        const locale = doc.data.locale;
        return Promise.all([
            this.zendesk.updateTranslationTitle(id, locale, doc.data.title),
            this.zendesk.updateTranslationBody(id, locale, doc.html)
        ]);
    }
    createTranslation(doc) {
        const articleId = this.article.source.data.articleId;
        return this.zendesk.createTranslation(articleId, doc.data.locale,
            { title: doc.data.title, body: doc.html });
    }
}

module.exports = Article;
module.exports.Publisher = Publisher;
